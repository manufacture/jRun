package ru.manufacture.java;

import com.sun.tools.javac.Main;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import ru.manufacture.util.Encoding;
import ru.manufacture.util.io.Location;
import ru.manufacture.util.io.PathType;

/**
 * @author Degtyarev Roman
 * @date 21.10.2015.
 */
public class Javac {
    public enum Option {
        ENCODING(Encoding.UTF8.code()),
        SOURCE_VERSION("1.7"),
        TARGET_VERSION("1.7"),
        GENERATE_DEBUG_FLAG("true"),
        NO_WARNING_FLAG("true");

        public String defaultValue;

        Option(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        public OptionValue is(String value) {
            return new OptionValue(this, value);
        }

        public String get(OptionValue ... options) {
            for (OptionValue option : options) {
                if (this == option.option) {
                    return option.value;
                }
            }
            return getDefaultValue();
        }
    }

    public static class OptionValue {
        private Option option;
        private String value;

        public OptionValue(Option option, String value) {
            this.option = option;
            this.value = value;
        }
    }

    private Path srcPath;
    private Path outPath;
    private ClassPath classPath;
    private boolean cleanAfterCompilation;

    public Javac(Path outPath, ClassPath classPath) {
        this(null, outPath, classPath);
    }

    public Javac(Path srcPath, Path outPath, ClassPath classPath) {
        this.srcPath = srcPath;
        this.outPath = outPath;
        this.classPath = classPath;
        this.cleanAfterCompilation = false;
    }

    public List<Location> compile(Location ... javaFiles) throws IOException {
        return compile(Arrays.asList(javaFiles));
    }

    public List<Location> compile(List<Location> javaFiles, OptionValue ... javacOptions) throws IOException {
        Path tmpSrcDir = Paths.get(System.getProperty("user.home"), ".jrun", "javac", "src");
        List<String> sources = getSrcPaths(tmpSrcDir, javaFiles);

        String[] options = initOptions(tmpSrcDir, sources, javacOptions);

        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        int result = Main.compile(options, printWriter);

        if (result != 0)
            throw new CompilationException(stringWriter.getBuffer().toString());

        return getClassFilesLocations(javaFiles);
    }

    private List<String> getSrcPaths(Path tmpSrcDir, List<Location> javaFiles) throws IOException {
        List<String> sources = new ArrayList<>(javaFiles.size());
        for (Location javaFile : javaFiles) {
            Path srcPath = javaFile.copyTo(tmpSrcDir);
            sources.add(srcPath.toString());
        }
        sources.addAll(getSrcLocationsExclude(javaFiles));
        return sources;
    }

    private List<Location> getClassFilesLocations(final List<Location> javaFiles) throws IOException {
        final List<Location> classes = new LinkedList<>();
        Files.walkFileTree(outPath, new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (!file.toString().endsWith(".class")) {
                    return super.visitFile(file, attrs);
                }
                String classFile = file.toAbsolutePath().toString();
                for (Location javaFileLocation : javaFiles) {
                    String javaFile = PathType.DIRECTORY.normalize(javaFileLocation.getResource());
                    String mask = javaFile.substring(0, javaFile.lastIndexOf(".java"));
                    if (classFile.endsWith(mask + ".class") || classFile.contains(mask + "$")) {
                        Path resource = outPath.relativize(file);
                        classes.add(new Location(PathType.DIRECTORY, outPath.toString(), resource.toString()));
                        return FileVisitResult.CONTINUE;
                    }
                }
                if (cleanAfterCompilation) {
                    Files.delete(file);
                }
                return super.visitFile(file, attrs);
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }
        });
        return classes;
    }

    private Collection<? extends String> getSrcLocationsExclude(final List<Location> excludeJavaFiles) throws IOException {
        if (null == srcPath) {
            return Collections.emptyList();
        }
        final Set<String> srcLocations = new HashSet<>();
        Files.walkFileTree(srcPath, new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (!file.toString().endsWith(".java")) {
                    return super.visitFile(file, attrs);
                }
                String filePath = file.toAbsolutePath().toString();
                for (Location exclude : excludeJavaFiles) {
                    String excludePath = PathType.DIRECTORY.normalize(exclude.getResource());
                    if (filePath.endsWith(excludePath)) {
                        return super.visitFile(file, attrs);
                    }
                }
                srcLocations.add(filePath);
                return super.visitFile(file, attrs);
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }
        });
        return srcLocations;
    }

    //javac -classpath <path> -d <directory> -encoding <encoding> -target <target> -g -sourcepath <sourcepath>
    private String[] initOptions(Path sourcePath, List<String> sources, OptionValue[] javacOptions) throws IOException {
        List<String> args = new LinkedList<>();
        String classPath = StringUtils.join(this.classPath.getAbsoluteLocations(), File.pathSeparator);
        args.add("-classpath");
        args.add(classPath);

        args.add("-encoding");
        args.add(Option.ENCODING.get(javacOptions));

        args.add("-source");
        args.add(Option.SOURCE_VERSION.get(javacOptions));

        args.add("-target");
        args.add(Option.TARGET_VERSION.get(javacOptions));

        if ("true".equalsIgnoreCase(Option.GENERATE_DEBUG_FLAG.get(javacOptions)))
            args.add("-g");

        if ("true".equalsIgnoreCase(Option.NO_WARNING_FLAG.get(javacOptions)))
            args.add("-nowarn");

        args.add("-sourcepath");
        args.add(sourcePath.toString() + (null == this.srcPath ? "" : File.pathSeparator + this.srcPath.toString()));

        if (!Files.exists(outPath)) {
            Files.createDirectories(outPath);
        }

        args.add("-d");
        args.add(outPath.toString());

        args.addAll(sources);

        String[] options = new String[args.size()];
        args.toArray(options);
        return options;
    }

    public void setCleanAfterCompilation(boolean cleanAfterCompilation) {
        this.cleanAfterCompilation = cleanAfterCompilation;
    }
}
