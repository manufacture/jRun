package ru.manufacture.java;

/**
 * @author Degtyarev Roman
 * @date 22.10.2015.
 */
public class CompilationException extends RuntimeException {
    public CompilationException(String message) {
        super(message);
    }

    public CompilationException(String message, Throwable cause) {
        super(message, cause);
    }
}
