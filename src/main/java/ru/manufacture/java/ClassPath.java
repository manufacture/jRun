package ru.manufacture.java;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import ru.manufacture.util.io.PathType;


import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.split;

/**
 * @author Degtyarev Roman
 * @date 15.10.2015.
 */
public class ClassPath {
    private static class Single {
        private static ClassPath instance = new ClassPath();
    }

    public static void refresh() {
        Single.instance = new ClassPath();
    }

    public static ClassPath newInstance(String baseDir, String ... locations) {
        return new ClassPath(baseDir, locations);
    }

    public static ClassPath instance() {
        return Single.instance;
    }

    private String baseDir;
    private List<String> locations;
    private List<String> absoluteLocations;

    private ClassPath() {
        this(System.getProperty(getBaseDirKey()), split(System.getProperty(getLocationsKey()), File.pathSeparatorChar));
    }

    public ClassPath(String baseDir, String ... locations) {
        this.baseDir = initBaseDir(baseDir);
        this.locations = Arrays.asList(normalize(locations));
        this.absoluteLocations = initAbsoluteLocations(this.baseDir, this.locations);
    }

    private String initBaseDir(String baseDir) {
        return isBlank(baseDir) ? System.getProperty("user.dir") : PathType.DIRECTORY.normalize(baseDir);
    }

    public static String[] normalize(String[] paths) {
        List<String> result = new LinkedList<>();
        for (int i = 0; i < paths.length; i++) {
            if (paths[i].contains(File.pathSeparator)) {
                String[] normal = normalize(split(paths[i], File.pathSeparator));
                result.addAll(Arrays.asList(normal));
            } else {
                result.add(PathType.DIRECTORY.normalize(paths[i]));
            }
        }
        return result.toArray(new String[result.size()]);
    }

    private List<String> initAbsoluteLocations(String baseDir, List<String> locations) {
        if (baseDir.endsWith(File.separator)) {
            baseDir = baseDir.substring(0, baseDir.lastIndexOf(File.separator));
        }
        List<String> result = new LinkedList<>();
        List<String> patterns = new LinkedList<>();
        for (String location : locations) {
            if (location.contains("*")) {
                patterns.add(location);
            } else if (Paths.get(location).isAbsolute()) {
                result.add(location);
            } else if (location.startsWith(File.separator))
                result.add(baseDir + location);
            else
                result.add(baseDir + File.separator + location);
        }
        if (!patterns.isEmpty()) {
            result.addAll(getMatchedPaths(baseDir, patterns));
        }
        return result;
    }

    private List<String> getMatchedPaths(final String baseDir, final List<String> pathPatterns) {
        final List<String> result = new LinkedList<>();
        try {
            Files.walkFileTree(Paths.get(baseDir), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    for (String pattern : pathPatterns) {
                        String syntaxAndPattern = getMatchPattern(baseDir, pattern);
                        boolean matches = matches(file, syntaxAndPattern);
                        if (matches) {
                            result.add(file.toString());
                        }
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException ignore) {}
        return result;
    }

    private boolean matches(Path file, String syntaxAndPattern) {
        Path fileName = file.getFileName();
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(syntaxAndPattern);
        return null != fileName && matcher.matches(file) && fileName.toString().toUpperCase().endsWith(".JAR");
    }

    private String getMatchPattern(String baseDir, String pattern) {
        String syntaxAndPattern;
        if (pattern.startsWith(File.separator)) {
            syntaxAndPattern = "glob:" + baseDir + pattern;
        } else {
            syntaxAndPattern = "glob:" + baseDir + File.separator + pattern;
        }
        return syntaxAndPattern.replace("\\", "/");
    }

    public static String getLocationsKey() {
        return ClassPath.class.getName() + ".locations";
    }

    public static String getBaseDirKey() {
        return ClassPath.class.getName() + ".baseDir";
    }

    public String getBaseDir() {
        return baseDir;
    }

    public List<String> getLocations() {
        return locations;
    }

    public List<String> getAbsoluteLocations() {
        return absoluteLocations;
    }
}
