package ru.manufacture.java;

/**
 * @author Degtyarev Roman
 * @date 16.10.2015.
 */
public class ClassLoadException extends RuntimeException {
    public ClassLoadException(String message) {
        super(message);
    }

    public ClassLoadException(String message, Throwable cause) {
        super(message, cause);
    }
}
