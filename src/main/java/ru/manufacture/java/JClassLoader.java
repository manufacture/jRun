package ru.manufacture.java;

import java.io.File;
import java.net.URL;
import java.util.List;
import ru.manufacture.util.io.Location;
import ru.manufacture.util.io.PathType;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 15.10.2015.
 */
public class JClassLoader extends ClassLoader {
    private static class Single {
        private static JClassLoader instance = new JClassLoader(JClassLoader.class.getClassLoader());
    }

    public static void refresh() {
        Single.instance = new JClassLoader(JClassLoader.class.getClassLoader());
    }

    public static JClassLoader instance() {
        return Single.instance;
    }

    public JClassLoader(ClassLoader parent) {
        super(parent);
    }

    @Override
    protected URL findResource(String resourceName) {
        try {
            Location location = lookup(resourceName);
            return location.getResourceAsUrl();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected Class<?> findClass(String className) throws ClassNotFoundException {
        if (isBlank(className))
            throw new ClassNotFoundException("Class name is null or empty");
        try {
            String classFileName = getClassFileName(className);
            Location location = lookup(classFileName);
            return defineClass(className, location);
        } catch (Exception e) {
            throw new ClassNotFoundException("Class not found: " + className, e);
        }
    }

    private Class<?> defineClass(String className, Location location) {
        try {
            byte[] bytes = location.getContent();
            return defineClass(className, bytes, 0, bytes.length);
        } catch (Throwable e) {
            throw new ClassLoadException("Can't load class: " + className + "; location: " + location, e);
        }
    }

    public static Class<?> classForName(String className) throws ClassNotFoundException {
        return instance().loadClass(className);
    }

    private static String getClassFileName(String className) {
        String classFile = className.replace(".", File.separator);
        return classFile + ".class";
    }

    public static Location lookup(String resource) {
        List<String> locations = ClassPath.instance().getAbsoluteLocations();
        for (String path : locations) {
            PathType pathType = PathType.of(path);
            if (pathType.exists(path, resource))
                return new Location(pathType, path, resource);
        }
        throw new NotFoundException("Resource not found: " + resource);
    }
}
