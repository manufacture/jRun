package ru.manufacture.java;

/**
 * @author Degtyarev Roman
 * @date 15.10.2015.
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
