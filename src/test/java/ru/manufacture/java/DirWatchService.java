package ru.manufacture.java;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

/**
 * @author Degtyarev Roman
 * @date 25.02.2016.
 */
public class DirWatchService {
    public static void watchDirectoryPath(Path path) {
        // Sanity check - Check if path is a folder
        try {
            Boolean isFolder = (Boolean) Files.getAttribute(path, "basic:isDirectory", NOFOLLOW_LINKS);
            if (!isFolder) {
                throw new IllegalArgumentException("Path: " + path + " is not a folder");
            }
        } catch (IOException ioe) {
            // Folder does not exists
            ioe.printStackTrace();
        }

        System.out.println("Watching path: " + path);

        // We obtain the file system of the Path
        FileSystem fs = path.getFileSystem();

        // We create the new WatchService using the new try() block
        try(WatchService service = fs.newWatchService()) {

            // We register the path to the service
            // We watch for creation events
            path.register(service, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);

            // Start the infinite polling loop
            WatchKey key = null;
            while(true) {
                key = service.take();
                Path watchable = (Path) key.watchable();
                // Dequeueing events
                WatchEvent.Kind<?> kind = null;
                for(WatchEvent<?> watchEvent : key.pollEvents()) {
                    // Get the type of the event
                    kind = watchEvent.kind();
                    if (OVERFLOW == kind) {
                        continue; //loop
                    } else if (ENTRY_CREATE == kind) {
                        // A new Path was created
                        Path dir = ((WatchEvent<Path>) watchEvent).context();
                        Path resolve = watchable.resolve(dir);
                        Boolean isFolder = (Boolean) Files.getAttribute(resolve, "basic:isDirectory", NOFOLLOW_LINKS);
                        if (isFolder) {
                            resolve.register(service, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);
                        }
                        // Output
                        System.out.println("New path created: " + resolve);
                    } else if (ENTRY_DELETE == kind) {
                        Path dir = ((WatchEvent<Path>) watchEvent).context();
                        Path resolve = watchable.resolve(dir);
                        // Output
                        System.out.println("path deleted: " + resolve);
                    } else if (ENTRY_MODIFY == kind) {
                        Path dir = ((WatchEvent<Path>) watchEvent).context();
                        Path resolve = watchable.resolve(dir);
                        // Output
                        System.out.println("path modified: " + resolve);
                    }
                }
                System.out.println("REFRESH ClassLoader");
                if(!key.reset()) {
                    break; //loop
                }
            }

        } catch(IOException ioe) {
            ioe.printStackTrace();
        } catch(InterruptedException ie) {
            ie.printStackTrace();
        }

    }

    public static void main(String[] args) throws IOException,
            InterruptedException {
        // Folder we are going to watch
        Path folder = Paths.get("target/watch");
        watchDirectoryPath(folder);
    }
}
