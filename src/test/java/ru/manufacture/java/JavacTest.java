package ru.manufacture.java;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.junit.Test;
import ru.manufacture.util.io.Location;
import ru.manufacture.util.io.PathType;

public class JavacTest {

    @Test
    public void testCompile() throws Exception {
        ClassPath classPath = ClassPath.newInstance(
                "target",
                "classes",
                "test-classes",
                System.getProperty("java.class.path")
        );
        Path src = null;//Paths.get("src\\main\\java");
        Path out = Paths.get("target\\out");
        Javac javac = new Javac(src, out, classPath);
        //javac.setCleanAfterCompilation(true);
        List<Location> classes = javac.compile(
                new Location(PathType.DIRECTORY, "src\\main\\java", "ru\\manufacture\\java\\Javac.java")
        );
        System.out.println(classes);
    }
}