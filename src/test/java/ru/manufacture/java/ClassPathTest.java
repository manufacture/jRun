package ru.manufacture.java;

import java.io.File;
import org.junit.Test;
import ru.manufacture.util.io.Location;
import ru.manufacture.util.io.PathType;

public class ClassPathTest {

    @Test
    public void testInstance() throws Exception {
        String userHome = System.getProperty("user.home");
        System.setProperty(ClassPath.getBaseDirKey(), userHome + File.separator + ".jaction");
        System.setProperty(ClassPath.getLocationsKey(), "/classes;/scripts/module[1].jar;resources;lib/**");
        ClassPath classPath = ClassPath.instance();
        for (String location : classPath.getAbsoluteLocations()) {
            System.out.println(PathType.of(location));
        }
        Location location = JClassLoader.lookup("org\\apache\\xerces\\impl\\XMLScanner.class");
        System.out.println(location);
        System.out.println(location.getResourceAsUrl());

        try {
            Location numberUtils = JClassLoader.lookup("org\\apache\\commons\\lang\\NumberUtils.class");
            System.out.println(numberUtils);
        } catch (Exception e) {
            String paths = System.getProperty(ClassPath.getLocationsKey());
            System.setProperty(ClassPath.getLocationsKey(), paths + File.pathSeparator + "commons/*");
            ClassPath.refresh();
            Location numberUtils = JClassLoader.lookup("org\\apache\\commons\\lang\\NumberUtils.class");
            System.out.println(numberUtils);
        }

        Class<?> aClass = JClassLoader.classForName("org.apache.commons.lang.NumberUtils");
        System.out.println(aClass);
        System.out.println(aClass.getClassLoader().getClass());
    }
}